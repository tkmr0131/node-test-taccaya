var mysql = require('mysql');

/*ローカル環境用
var dbConfig = {
  host: '127.0.0.1',
  user: 'root',
  password: '',
  database: 'bulletin_board'
};
*/

//リリース用
var dbConfig = {
  host: '[host]',
  user: '[user]',
  password: '[password]',
  database: '[database]'
};

var connection = mysql.createConnection(dbConfig);

// これはHerokuのMySQLのためのハックです。
setInterval(function() {
  connection.query('SELECT 1');
}, 5000);

module.exports = connection;
